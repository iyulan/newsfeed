Newsfeed::Application.routes.draw do

  resources :news_items, path: 'feed'

  get "welcome/index"
  get "omniauth_callbacks/vkontakte"
  devise_for :users,  path: "auth", path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', unlock: 'unblock', sign_up: 'register' }, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }
  root 'welcome#index'

  #get '/feed', to: 'news_items#index'
  get '/profile', :to => "users#profile", :as => :profile

end
