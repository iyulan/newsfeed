class ChangeTextFieldForNews < ActiveRecord::Migration
  def change
    change_table :news_items do |t|
      t.change :text, :text
    end
  end
end
