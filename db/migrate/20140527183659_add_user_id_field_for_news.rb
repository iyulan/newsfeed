class AddUserIdFieldForNews < ActiveRecord::Migration
  def change
  	change_table(:news_items) do |t|
  	  t.integer :user_id
  	end
  end
end
