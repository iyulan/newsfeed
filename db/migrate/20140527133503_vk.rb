class Vk < ActiveRecord::Migration
  def change
    change_table(:users) do |t|
      t.string :username 
      t.string :provider
      t.string :url
      t.text :about
    end
  end
end
