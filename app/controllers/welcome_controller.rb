class WelcomeController < ApplicationController
  def index
    redirect_to news_items_path if user_signed_in?
  end
end
