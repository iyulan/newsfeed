class NewsItem < ActiveRecord::Base
  validates_presence_of :title, :text

  belongs_to :user
end
