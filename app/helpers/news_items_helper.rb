module NewsItemsHelper

  def date_format(news_date)
    date_diff = ((DateTime.now - news_date.to_datetime).to_i/1.day)
    case date_diff 
      when 1
        s = "Yesterday, "
      when 0
        s = "Today, "
      else
        s = news_date.strftime('%d.%m') + ' ,'    
    end 
    s + news_date.strftime('%H:%M')
  end
  
end
